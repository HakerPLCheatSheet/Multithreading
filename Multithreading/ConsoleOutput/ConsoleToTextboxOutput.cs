﻿using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Multithreading.ConsoleOutput
{
    class ConsoleToTextboxOutput : TextWriter
    {
        readonly TextBlock _output = null; //Textbox used to show Console's output.

        /// <summary>
        /// Custom TextBox-Class used to print the Console output.
        /// </summary>
        /// <param name="output">Textbox used to show Console's output.</param>
        public ConsoleToTextboxOutput(TextBlock output)
        {
            _output = output;
            _output.TextWrapping = TextWrapping.Wrap;
        }

        //<summary>
        //Appends text to the textbox and to the logfile
        //</summary>
        //<param name="value">Input-string which is appended to the textbox.</param>
        public override void Write(char value)
        {
            base.Write(value);
            _output.Inlines.Add(value.ToString());//Append char to the textbox
            _output.Inlines.Add(new LineBreak());
        }

        public override Encoding Encoding => System.Text.Encoding.UTF8;
    }
}
