﻿using System.Windows;
using Multithreading.TaskExample;
using Multithreading.ThreadExample;

namespace Multithreading
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void BtnThreadFirst_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadFirst();
            tt.Start();
        }

        private void BtnThreadBackground_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadBackground();
            tt.Start();
        }

        private void BtnThreadSynchronization_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadSynchronizationOne();
            tt.Start();
        }

        private void BtnThreadSynchronizationSecound_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadSynchronizationSecound();
            tt.Start();
        }

        private void BtnThreadSynchronizationThree_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadSynchronizationThree();
            tt.Start();
        }

        private void BtnThreadSplitOperation1_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadSplitOperation();
            tt.Start();
        }

        private void BtnThreadSplitOperation2_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadMultipleAccessToData();
            tt.Start();
        }

        private void BtnSemaphore_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadSemaphore();
            tt.Start();
        }

        private void BtnMutex_Click(object sender, RoutedEventArgs e)
        {
            IThreadExample tt = new ThreadMutex();
            tt.Start();
        }

        private void BtnTaskWaitingForAll_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskWaitingForAll();
            tt.Start();
        }

        private void BtnTaskWaitingForOne_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskWaitAny();
            tt.Start();
        }

        private void BtnTaskUseAction_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskUseAction();
            tt.Start();   
        }

        private void BtnTaskSynchronous_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskSynchronous();
            tt.Start();
        }

        private void BtnTaskAsynchronous_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskAsynchronous();
            tt.Start();
        }

        private void BtnTaskFactory_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskFactory();
            tt.Start();
        }

        private void BtnTaskReturnTypesAsMethod_Click(object sender, RoutedEventArgs e)
        {
            TaskReturnTypesAsMethod tt = new TaskReturnTypesAsMethod();
            tt.Start();

            TxbTaskReturnTypesAsMethod.Text = tt.NewString;
        }

        private void BtnTaskParallel_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskParallel();
            tt.Start();
        }

        private void BtnTaskCancelationToken_Click(object sender, RoutedEventArgs e)
        {
            ITaskExample tt = new TaskCancelationToken();
            tt.Start();
        }
    }
}
