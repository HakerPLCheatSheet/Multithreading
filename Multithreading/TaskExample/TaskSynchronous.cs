﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskSynchronous : ITaskExample
    {
        public void Start()
        {
            Console.WriteLine("Start tasks");

            //Task<type> - type = return data
            Task<int> task = Task<int>.Run(() =>
            {
                int result = 0;

                for (int i = 0; i < 10000; i++)
                {
                    result += i;
                    Console.WriteLine($"Current result: {result}");
                }

                Thread.Sleep(3000);

                return result;
            });

            Console.WriteLine($"Synchronous Task Complete! Result: {task.Result.ToString()}");
        }
    }
}
