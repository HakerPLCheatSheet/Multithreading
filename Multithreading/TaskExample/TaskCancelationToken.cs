﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskCancelationToken : ITaskExample
    {
        //pomaga przy przerwaniu operacji w taskach (jesli cos sie posypalo)
        private CancellationTokenSource _cancellationTokenSource;

        public void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();

            try
            {
                //_cancellationTokenSource.Cancel();
                Task.Factory.StartNew(() => DoSomething(1, 4000, _cancellationTokenSource.Token)).ContinueWith((prevTask) => DoSomethingImportantId(1, 1000, _cancellationTokenSource.Token));
                Task.Factory.StartNew(() => DoSomething(2, 2500, _cancellationTokenSource.Token)).ContinueWith((prevTask) => DoSomethingImportantTask(prevTask, 2500, _cancellationTokenSource.Token));

                Thread.Sleep(2000);
                _cancellationTokenSource.Cancel();
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine($"Exception throw cancelation token type: {ex.GetType()}");
                throw;
            }
        }

        private void DoSomethingImportantTask(Task prevTask, int sleepTime, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine($"DoSomethingImportantTask - currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} Cancelation token, throw exception");
                token.ThrowIfCancellationRequested();
            }

            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} done work on important task");
        }

        private void DoSomething(int id, int sleepTime, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine($"DoSomething - Task {id} currentTaskId: {Task.CurrentId} Cancelation token, throw exception");
                token.ThrowIfCancellationRequested();
            }

            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work");
        }

        private void DoSomethingImportantId(int id, int sleepTime, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine($"DoSomethingImportantId - Task {id} currentTaskId: {Task.CurrentId} Cancelation token, throw exception");
                token.ThrowIfCancellationRequested();
            }

            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work on important task");
        }
    }
}
