﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskParallel : ITaskExample
    {
        public void Start()
        {
            Console.WriteLine("Start Parallel");

            List<int> intList = new List<int>() { 1, 2, 3, 5, 8, 7, 6, 9, 14, 55, 98, 63, 47, 89, 62, 48, 52, 17, 64 };

            Parallel.ForEach(intList, (i) => Console.WriteLine($"foreach {i}"));

            Parallel.For(0, 100, (i) => Console.WriteLine($"for {i}"));

            Console.WriteLine("End Parallel");
        }

        public void Start()
        {
            Random random = new Random(123);

            List<int> numbers = new List<int>();

            Console.WriteLine($"Paraller foreach start");

            for (int i = 0; i < 100; i++)
                numbers.Add(random.Next(0, 100));

            Console.WriteLine($"Numbers: {string.Join(",", numbers)}");

            //that block thread to end all task
            Parallel.ForEach(numbers, (i) => Console.WriteLine($"Paraller foreach write number {i}"));

            Console.WriteLine($"Paraller foreach end");

            //Task.Factory.StartNew(() => DoSomething(1, 2000)).ContinueWith((prevTask) => DoSomethingImportant(1, 1000));
            //Task.Factory.StartNew(() => DoSomething(2, 3000)).ContinueWith((prevTask) => DoSomethingImportant(prevTask, 500));
        }

        private void DoSomethingImportant(Task prevTask, int sleepTime)
        {
            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} done work on important task");
        }

        private void DoSomething(int id, int sleepTime)
        {
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work");
        }

        private void DoSomethingImportant(int id, int sleepTime)
        {
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work on important task");
        }
    }
}
