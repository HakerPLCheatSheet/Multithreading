﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskWaitAny : ITaskExample
    {
        private int _taskNumber = 10;

        public void Start()
        {
            Task[] tasks = new Task[_taskNumber];

            Random random = new Random();

            Console.WriteLine("Start tasks");

            for (int i = 0; i < _taskNumber; i++)
                tasks[i] = Task.Run(() => Thread.Sleep(random.Next(500, 3000)));

            try
            {
                //Wait for one task to complete
                int index = Task.WaitAny(tasks);

                Console.WriteLine($"Task #{tasks[index].Id} completed first");

                Console.WriteLine("Status all Task");

                foreach (var t in tasks)
                    Console.WriteLine($"Task #{t.Id}: {t.Status}");
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");

                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine($"{ae.Message}");
            }
        }
    }
}
