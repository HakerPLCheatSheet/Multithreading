﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskFactory : ITaskExample
    {
        public void Start()
        {
            Task.Factory.StartNew(() => DoSomething(1, 2000)).ContinueWith((prevTask) => DoSomethingImportant(1, 1000));
            Task.Factory.StartNew(() => DoSomething(2, 3000)).ContinueWith((prevTask) => DoSomethingImportant(prevTask, 500));
        }

        private void DoSomethingImportant(Task prevTask, int sleepTime)
        {
            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"currentTaskId: {Task.CurrentId} Task prevTaskID: {prevTask.Id} isCompleted: {prevTask.IsCompleted} done work on important task");
        }

        private void DoSomething(int id, int sleepTime)
        {
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work");
        }

        private void DoSomethingImportant(int id, int sleepTime)
        {
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} start work on important task");
            Thread.Sleep(sleepTime);
            Console.WriteLine($"Task {id} currentTaskId: {Task.CurrentId} done work on important task");
        }
    }
}
