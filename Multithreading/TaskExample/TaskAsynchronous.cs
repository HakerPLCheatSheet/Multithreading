﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskAsynchronous : ITaskExample
    {
        public void Start()
        {
            TaskAsync();
        }

        private async void TaskAsync()
        {
            Console.WriteLine("Start tasks");

            //Task<type> - type = return data
            Task<int> task = Task<int>.Run(() =>
            {
                int result = 0;

                for (int i = 0; i < 10000; i++)
                {
                    result += i;
                    Console.WriteLine($"Current result: {result}");
                }

                Thread.Sleep(3000);

                return result;
            });

            //await that waith but continue other action (I will be back heere)
            //await on task
            await Task.WhenAll(task);

            Console.WriteLine($"Asynchronous Task Complete! Result: {task.Result.ToString()}");
        }
    }
}
