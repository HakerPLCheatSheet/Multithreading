﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskReturnTypesAsMethod : ITaskExample
    {
        private string _newString;
        public string NewString => _newString;

        public void Start()
        {
            TaskAsync();
        }

        private void TaskAsync()
        {
            Console.WriteLine("Start tasks");

            //Task<type> - type = return data
            Task<string> task = Task<string>.Run(() =>
            {
                string letters = "abcdefghijklmnopqrstuvwxyz";
                Random random = new Random();
                String newString = string.Empty;
                

                for (int i = 0; i < 10; i++)
                {
                    int next = random.Next(0, letters.Length);
                    newString += $"{letters[next]}{next.ToString()}";
                }

                return newString;
            });

            Task.WhenAll(task);

            _newString = task.Result;

            Console.WriteLine($"Asynchronous Task Complete! Result: {task.Result.ToString()}");
        }
    }
}
