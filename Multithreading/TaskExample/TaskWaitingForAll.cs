﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskWaitingForAll : ITaskExample
    {
        private int _taskNumber = 10;

        public void Start()
        {
            Task[] tasks = new Task[_taskNumber];

            Console.WriteLine("Start tasks");

            for (int i = 0; i < _taskNumber; i++)
                tasks[i] = Task.Run(() => Thread.Sleep(2000));

            try
            {
                //Wait for all tasks to complete
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");

                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine($"{ae.Message}");
            }

            Console.WriteLine("End tasks");
            foreach (var t in tasks)
                Console.WriteLine($"Task #{t.Id}: {t.Status}");
        }
    }
}
