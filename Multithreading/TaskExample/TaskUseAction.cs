﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading.TaskExample
{
    public class TaskUseAction : ITaskExample
    {
        private int _taskNumber = 10;

        private List<string> _selectedItems;
        private List<string> _store;

        public void Start()
        {
            _selectedItems = new List<string>();
            _store = new List<string>
            {
                "Pickaxe", "Sword", "9mm Pistol", "Shield", "Helmet", ".30-06 Rifle",
                "TNT", "Granade", "Lead Pipe", "Shovel"
            };

            Task[] tasks = new Task[_taskNumber];
            var random = new Random();


            Console.WriteLine("Start tasks");

            //Action (delegate) return allways void
            Action<object> action = (object obj) =>
            {
                _selectedItems.Add($@"You've recieved: {obj.ToString()}, Selected by Thread {Thread.CurrentThread.ManagedThreadId} in Task Id {Task.CurrentId}");
            };


            for (int i = 0; i < _taskNumber; i++)
            {
                string newItem = _store[random.Next(0, _store.Count)];
                tasks[i] = new Task(action, newItem);
                tasks[i].Start();
                tasks[i].Wait();
            }

            foreach (string item in _selectedItems)
                Console.WriteLine(item);

            Console.WriteLine("End tasks");
        }
    }
}
