﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public struct LenghtData
    {
        public int Processor;
        public int From;
        public int To;
    }

    public class ThreadSplitOperation : IThreadExample
    {
        private byte[] _values = new byte[999999999];
        private long[] _portionResult;

        public void Start()
        {
            GenerateInts();

            Sum();
            ThreadsSum();
        }

        private void Sum()
        {
            Console.WriteLine($"Summing...");

            long total1 = 0;

            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (int i = 0; i < _values.Length; i++)
                total1 += _values[i];

            watch.Stop();
            Console.WriteLine($"Total value is: {total1}, time sum: {watch.Elapsed}");
        }

        private void ThreadsSum()
        {
            Console.WriteLine($"Summing Thread...");

            //Environment.ProcessorCount count of cpu
            _portionResult = new long[Environment.ProcessorCount];

            Thread[] threadsSum = new Thread[Environment.ProcessorCount];

            for (int i = 0; i < threadsSum.Length; i++)
                threadsSum[i] = new Thread(SumPortion);

            int from = 0;
            int count = 0;

            LenghtData[] lenghtDatas = new LenghtData[threadsSum.Length];

            for (int i = 0; i < threadsSum.Length; i++)
            {
                threadsSum[i] = new Thread(SumPortion);

                lenghtDatas[i].Processor = count;
                lenghtDatas[i].From = from;

                if (count + 1 != Environment.ProcessorCount)
                    lenghtDatas[i].To = lenghtDatas[i].From + _values.Length / Environment.ProcessorCount;
                else
                    lenghtDatas[i].To = lenghtDatas[i].From + _values.Length - ((_values.Length / Environment.ProcessorCount) * (Environment.ProcessorCount - 1));

                from = lenghtDatas[i].To;
                //Console.WriteLine($"lenghtData.From: {lenghtDatas[i].From}, lenghtData.To: {lenghtDatas[i].To}");

                count++;
            }

            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (int i = 0; i < threadsSum.Length; i++)
                threadsSum[i].Start(lenghtDatas[i]);

            //threadsSum[i].Join() zatrzymuje odtwarzanie kodu do momentu zakonczenia watku
            for (int i = 0; i < Environment.ProcessorCount; i++)
                threadsSum[i].Join();

            long total2 = 0;

            foreach (var item in _portionResult)
                total2 += item;

            watch.Stop();
            Console.WriteLine($"Total value is: {total2}, time sum: {watch.Elapsed}");
        }

        private void GenerateInts()
        {
            var rand = new Random(987);

            for (int i = 0; i < _values.Length; i++)
                _values[i] = (byte) rand.Next(10);
        }

        private void SumPortion(object data)
        {
            if (!(data is LenghtData lenghtData))
                return;

            long sum = 0;

            for (int i = lenghtData.From; i < lenghtData.To; i++)
                sum += _values[i];

            _portionResult[lenghtData.Processor] = sum;
        }
    }
}
