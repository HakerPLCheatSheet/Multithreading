﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public class ThreadMultipleAccessToData : IThreadExample
    {
        private Queue<int> _numbersQueue = new Queue<int>();
        private const int _numThread = 3;
        private int[] _portionResult = new int[_numThread];

        public void Start()
        {
            Thread generateThread = new Thread(GenerateInts);

            Thread[] sumThread = new Thread[_numThread];

            generateThread.Start();

            for (int i = 0; i < sumThread.Length; i++)
                sumThread[i] = new Thread(SumPortion);

            for (int i = 0; i < sumThread.Length; i++)
                sumThread[i].Start(i);

            foreach (var threadJoin in sumThread)
                threadJoin.Join();

            int sum = 0;

            foreach (var number in _portionResult)
                sum += number;

            Console.WriteLine($"Sum :{sum}");
        }

        private void GenerateInts()
        {
            var rand = new Random(987);

            for (int i = 0; i < 10; i++)
            {
                int number = rand.Next(10);

                lock(_numbersQueue)
                    _numbersQueue.Enqueue(number);

                Console.WriteLine($"Generate {number}");

                Thread.Sleep(rand.Next(1000));
            }
        }

        private void SumPortion(object threadNumber)
        {
            if (!(threadNumber is int numberThread))
                return;

            DateTime time = DateTime.Now;
            int sum = 0;
            while ((DateTime.Now - time).Seconds < 11)
            {
                int numberFromQueue = 0;

                lock (_numbersQueue)
                {
                    if (_numbersQueue.Count > 0)
                        numberFromQueue = _numbersQueue.Dequeue();
                    else
                        continue;
                }

                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} take number {numberFromQueue}");

                sum += numberFromQueue;
            }

            _portionResult[numberThread] = sum;
        }
    }
}
