﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    class ThreadBackground : IThreadExample
    {
        public void Start()
        {
            Thread tt = new Thread(() =>
            {
                for (int i = 0; i < Environment.ProcessorCount; i++)
                {
                    var oo = new Thread(StartThreadRun)
                    {
                        IsBackground = true // when main thread will be close that threed olso will be close
                    };
                    oo.Start();
                }
            });
            
            tt.Start();
        }

        private void StartThreadRun()
        {
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine(
                    $"Background {i} Thread {Thread.CurrentThread.ManagedThreadId} --- Thread.CurrentThread.IsBackground {Thread.CurrentThread.IsBackground}");
                Thread.Sleep(500);
            }
        }
    }
}
