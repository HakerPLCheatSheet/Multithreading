﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    class ThreadFirst : IThreadExample
    {
        public void Start()
        {
            Thread t = new Thread(() =>
            {
                for (int i = 0; i <= 10000; i++)
                    Console.WriteLine($"Count {i} on thread {Thread.CurrentThread.ManagedThreadId}");
            });

            t.Start();

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                Thread tt = new Thread(StartThreadRun);
                tt.Start(i);
            }
        }

        private void StartThreadRun(object ii)
        {
            for (int i = 0; i <= 10000; i++)
                Console.WriteLine($"Count {i} on thread {Thread.CurrentThread.ManagedThreadId} --- Environment.ProcessorCount {ii}");
        }
    }
}
