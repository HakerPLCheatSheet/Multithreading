﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public class ThreadSemaphore : IThreadExample
    {
        private bool _semCreated;
        private int _semPermision = 3;
        private int _totalThread = 20;

        //Semaphore have permision to go inside code (like lock but here we can have more then one)
        //it can be system wide (shared by multiple processes).
        //allows x number of threads to enter, this can be used for example to limit the number of cpu, 
        //io or ram intensive tasks running at the same time.
        private Semaphore _semaphore;

        public void Start()
        {
            //first: how much we have not use permision, secound: max of permision
            //Name "sample_sem" shared between processes
            _semaphore = new Semaphore(_semPermision, _semPermision, "sample_sem", out _semCreated);

            if (_semCreated)
                Console.WriteLine("Semaphore sample_sem was create");

            for (int i = 0; i < _totalThread; i++)
                new Thread(SemaphoreTest).Start(i);
        }

        private void SemaphoreTest(object threadId)
        {
            Console.WriteLine($"Thread {threadId} try access semaphore");

            //get permision from semaphore or wait for permision
            _semaphore.WaitOne();

            Console.WriteLine($"Thread {threadId} get permision from semaphore");

            Thread.Sleep(3000);

            Console.WriteLine($"Thread {threadId} return permision to semaphore");

            //return permision to semaphore
            _semaphore.Release();
        }
    }
}
