﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public class ThreadSynchronizationSecound : IThreadExample
    {
        private Random _rand = new Random();
        private object _lock = new object();

        public void Start()
        {
            for (int i = 0; i < 5; i++)
            {
                new Thread(IncrementCount).Start();
            }
        }

        private void IncrementCount()
        {
            Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} Before lock");

            lock (_lock)
            {
                Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} In Before sleep lock");

                Thread.Sleep(_rand.Next(2000));

                Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} In After sleep lock");
            }

            Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} After lock");
        }
    }
}
