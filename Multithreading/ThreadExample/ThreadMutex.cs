﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public class ThreadMutex : IThreadExample
    {
        private int _counter = 0;
        //A mutex is the same as a lock but it can be system wide (shared by multiple processes).
        private Mutex _mutex;

        public void Start()
        {
            //create Mutex. Name "mutex" shared between processes
            _mutex = new Mutex(false, "mutex");


            Thread t1 = new Thread(CounterAdd);
            Thread t2 = new Thread(CounterAdd);

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Console.WriteLine($"Counter: {_counter}");
        }

        private void CounterAdd()
        {
            for (int i = 0; i < 100000; i++)
            {
                _mutex.WaitOne();
                _counter++;
                _mutex.ReleaseMutex();
            }
        }
    }
}
