﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    public class ThreadSynchronizationOne : IThreadExample
    {
        private int _count = 0;
        private object _lock = new object();

        public void Start()
        {
            Thread one = new Thread(IncrementCount);
            Thread secound = new Thread(IncrementCount);

            one.Start();
            Thread.Sleep(500);
            secound.Start();
        }

        private void IncrementCount()
        {
            for (int i = 0; i < 50; i++)
            {
                lock (_lock)
                {
                    var cc = _count;
                    Thread.Sleep(1000);
                    _count = cc + 1;

                    Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} increment to {_count}, cc {cc}");
                }

                Thread.Sleep(1000);
            }
        }
    }
}
