﻿using System;
using System.Threading;

namespace Multithreading.ThreadExample
{
    /*
     * Monitor = lock z ThreadSynchronizationSecound
     */
    public class ThreadSynchronizationThree : IThreadExample
    {
        private Random _rand = new Random();
        private object _lock = new object();

        public void Start()
        {
            for (int i = 0; i < 5; i++)
            {
                new Thread(IncrementCount).Start();
            }
        }

        private void IncrementCount()
        {
            Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} Before Monitor");

            bool lockTaken = false;
            //lockTaken nie jest wymagany, na 99.9% zawsze lock bedzie zabrany
            Monitor.Enter(_lock, ref lockTaken);

            try
            {
                Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} In Before sleep Monitor");

                Thread.Sleep(_rand.Next(2000));

                Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} In After sleep Monitor");
            }
            finally
            {
                if(lockTaken)
                    Monitor.Exit(_lock);
            }

            Console.WriteLine($"Thread ID {Thread.CurrentThread.ManagedThreadId} After Monitor");
        }
    }
}
