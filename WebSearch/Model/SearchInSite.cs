﻿using System.Collections.Generic;

namespace WebSearch.Model
{
    class SearchInSite
    {
        public static List<string> Urls = new List<string>()
        {
            "http://www.cnn.com/",
            "http://www.nytimes.com/",
            "http://www.foxnews.com/",
            "http://www.msnbc.msn.com/",
            "http://news.cnet.com/",
            "http://www.cbsnews.com/",
            "http://abcnews.go.com/",
            "http://news.yahoo.com/",
            "http://www.npr.org/sections/news/",
            "http://www.boston.com/news/",
            "http://www.dallasnews.com/",
            "http://www.sciencenews.org/",
            "http://www.reuters.com/",
            "http://www.newscorp.com/",
            "http://www.nfl.com/news",
            "http://www.suntimes.com/news/",
            "http://www.zdnet.com/news",
            "http://www.theonion.com/",
            "http://www.wired.com/",
            "http://www.chicagotribune.com/news/local/",
            "http://www.bloomberg.com/news/",
            "http://www.nydailynews.com/index.html",
            "http://www.mercurynews.com/",
        };
    }
}
