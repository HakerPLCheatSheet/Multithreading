﻿namespace WebSearch.Model
{
    class DownloadResult
    {
        public string Url { get; set; }
        public string Data { get; set; }
    }
}
