﻿namespace WebSearch.Model
{
    class SearchStatus
    {
        public string CurrentAction { get; set; }
        public string CurrentTarget { get; set; }
        public int DownloadCount { get; set; }

        public void Clear()
        {
            this.CurrentAction = "Idle";
            this.CurrentTarget = "";
            this.DownloadCount = 0;
        }
    }
}
