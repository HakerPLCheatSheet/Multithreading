﻿namespace WebSearch.Model
{
    class SearchResult
    {
        public string ArticleTitle { get; set; }
        public string Term { get; set; }
        public string CalloutText { get; set; }
        public string Url { get; set; }

        public override string ToString()
        {
            return $"Search={Term}, Title={ArticleTitle}, Callout={CalloutText}, Url={Url}";
        }
    }
}
