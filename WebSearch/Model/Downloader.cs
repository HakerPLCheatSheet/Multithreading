﻿using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace WebSearch.Model
{
    class Downloader
    {
        // nowa wersja
        public async static Task<string> DownloadHtmlTaskAsync(string url)
        {
            Debug.WriteLine("Starting download for " + url);

            WebClient client = new WebClient();
            string download = await client.DownloadStringTaskAsync(url);

            Debug.WriteLine("Finished download of " + url);

            return download;
        }

        // stara wersja
        public static string DownloadHtml(string url)
        {
            Debug.WriteLine("Starting download for " + url);

            WebClient client = new WebClient();
            var download = client.DownloadString(url);

            Debug.WriteLine("Finished download of " + url);

            return download;
        }
    }
}
