﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using JulMar.Windows.Mvvm;
using WebSearch.Model;

namespace WebSearch.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public ICommand SearchCommand { get; set; }
        public ICommand OpenSelectedSiteCommand { get; set; }

        public ObservableCollection<SearchResult> SearchResults { get; set; }
        public SearchResult SelectedResult { get; set; }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                OnPropertyChange(nameof(SearchText));
            }
        }

        private string _statusText;
        public string StatusText
        {
            get => _statusText;
            set
            {
                _statusText = value;
                OnPropertyChange(nameof(StatusText));
                // Forces the CommandManager to raise the RequerySuggested event.
                CommandManager.InvalidateRequerySuggested();
            }
        }

        private bool _isSearching;
        public bool IsSearching
        {
            get => _isSearching;
            set
            {
                _isSearching = value;
                CommandManager.InvalidateRequerySuggested();
            }
        }

        private SearchStatus _searchStatus;

        public MainWindowViewModel()
        {
            _searchText = "";
            _statusText = "idle";

            _searchStatus = new SearchStatus();
            SearchCommand = new DelegateCommand(OnSearch, CanSearch);
            OpenSelectedSiteCommand = new DelegateCommand(OnVisit, CanVisit);
            SearchResults = new ObservableCollection<SearchResult>();
            UpdateStats();
        }

        private async void OnSearch()
        {
            IsSearching = true;
            SearchResults.Clear();

            _searchStatus.DownloadCount = SearchInSite.Urls.Count;

            List<Task<DownloadResult>> tasks = new List<Task<DownloadResult>>();

            foreach (string url in SearchInSite.Urls)
                tasks.Add(DownloadWithUrltrackTaskAsync(url));

            while (tasks.Count > 0)
            {
                //czekamy az ktoras ze stron nam odpowie
                Task<DownloadResult> finishTask = await Task.WhenAny(tasks);
                // jesli ktoras ze stron odpowiedziala to usuwamy ja z listy i obrabiamy dane ktore dostalismy
                tasks.Remove(finishTask);

                _searchStatus.DownloadCount--;
                _searchStatus.CurrentTarget = finishTask.Result.Url;
                _searchStatus.CurrentAction = "Searching text";

                // nie powoduje zawieszenia sie okienka w chwili aktualizacji listy
                // przerobienie html zajmuje troszke czasu
                await Task.Run(() => FillListSearchResult(finishTask.Result));

                IsSearching = tasks.Count > 0;
            }
        }

        private bool CanSearch()
        {
            return
                IsSearching == false &&
                SearchText != null &&
                SearchText.Trim().Length > 0;
        }

        private void OnVisit()
        {
            Process.Start(SelectedResult.Url);
        }

        private bool CanVisit()
        {
            return SelectedResult != null;
        }

        private void FillListSearchResult(DownloadResult downloadResult)
        {
            List<SearchResult> terms = SearchEngine.Search(SearchText, downloadResult.Data, downloadResult.Url);

            // uaktualnia liste w innym watku
            App.Current.Dispatcher.Invoke(() =>
            {
                foreach (SearchResult result in terms)
                    SearchResults.Add(result);
            });
        }

        private async void UpdateStats()
        {
            while (true)
            {
                // pozwala na przeskoczenie do nastepnych zadan
                // nie powoduje zawieszenia sie programu w nieskonczonej petli
                await Task.Delay(100);

                if (_searchStatus.DownloadCount > 0)
                {
                    StatusText = string.Format("[{0} pending] {1} on {2}.",
                        _searchStatus.DownloadCount,
                        _searchStatus.CurrentAction,
                        _searchStatus.CurrentTarget);
                }
                else
                {
                    StatusText = "Current action: Idle";
                }
            }
        }

        // pobieramy dane ze stron
        private async Task<DownloadResult> DownloadWithUrltrackTaskAsync(string url)
        {
            DownloadResult result = new DownloadResult();
            result.Url = url;
            result.Data = await Downloader.DownloadHtmlTaskAsync(url);

            return result;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChange([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
